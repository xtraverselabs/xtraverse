from flask import Flask, render_template, session, redirect, url_for, current_app, g, request
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, Length
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin, LoginManager, login_required, login_user, logout_user, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps


#  Instantiating a Flask application called "app"
app = Flask(__name__)

#  SQL Alchemy database URI
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://tshiamo:6Ec^#U#v!!Dp@154.0.163.160/xtraverse'

#  Disabling SQL_ALCHEMY_TRACK_MODIFICATIONS because of significant overhead it adds
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

#  Instantiating SQLAlchemy db object
db = SQLAlchemy(app)

#  Secret key is required to use CSRF
app.config['SECRET_KEY'] = 'g!WvyNHAn6o(!oa94J%9'

""" MODELS AND DATABASE """


# Define the User data model
class User(db.Model, UserMixin):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Unicode(50), nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password = password


# Define the UserScores association model
class UsersScores(db.Model):
    __tablename__ = 'users_scores'
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id', ondelete='CASCADE'))
    highscore = db.Column(db.Integer(), default=0)
    points = db.Column(db.Integer(), default=0)

    def __init__(self, user_id, highscore, points):
        self.user_id = user_id
        self.highscore = highscore
        self.points = points


""" FORMS """


# LoginForm
class LoginForm(FlaskForm):
    errormsg_user = "Please insert a username between with 5 to 15 characters"
    errormsg_pass = "Please insert a password between 8 to 80 characters"
    username = StringField('Username', validators=[InputRequired(errormsg_user), Length(min=5, max=15)])
    password = PasswordField('Password', validators=[InputRequired(errormsg_pass), Length(min=8, max=80)])


# LoginForm
class RegistrationForm(FlaskForm):
    errormsg_user = "Please insert a username between 5 to 15 characters"
    errormsg_pass = "Please insert a password between 8 to 80 characters"
    errormsg_conf = "Make sure that this matches the password you entered above"
    username = StringField('Username', validators=[InputRequired(errormsg_user), Length(min=5, max=15)])
    password = PasswordField('Password', validators=[InputRequired(errormsg_pass), Length(min=8, max=80)])
    confirm = PasswordField('Confirm Password', validators=[InputRequired(errormsg_conf), Length(min=8, max=80)])


""" ROUTES """


#  Route for login form
@app.route('/', methods=['GET', 'POST'])
def login():
    signinform = LoginForm()
    if signinform.validate_on_submit():
        # We need to see if user exists, so we start by querying for user
        user = User.query.filter_by(username=signinform.username.data).first()

        # If the user actually exists, let's check the password
        if user:
            if check_password_hash(user.password, signinform.password.data):
                return redirect(url_for('flight'))

    return render_template('login.html', form=signinform)


#  Route for registration form
@app.route('/register', methods=['GET', 'POST'])
def register():
    registerform = RegistrationForm()
    if registerform.validate_on_submit() and registerform.password.data == registerform.confirm.data:
        hashed_pass = generate_password_hash(registerform.password.data, method='sha256')
        new_user = User(username=registerform.username.data, password=hashed_pass)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('flight'))
    return render_template('register.html', form=registerform)


#  Route for the flight page
@login_required
@app.route('/xtraverse-flight')
def flight():
    return render_template('flight.html')


#  Route for the xplore page (the bonus scene)
@login_required
@app.route('/xplore')
def xplore():
    return render_template('xplore.html')


#  Route for the game
@login_required
@app.route('/xflight')
def xflight():
    return render_template('xflight.html')



@login_required
@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


""" RUN SERVER """
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
