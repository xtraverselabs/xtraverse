var counter = 3;

ship.addEventListener('collide', function (e) {
	saveScore();
	endGame();
});

function saveScore(argument) {
	scoreText.pause();	
	let currentScore = parseInt(scoreText.components.score.getScore());
	if (currentScore > highScore) localStorage.setItem('xtraverseFlightScore', currentScore);
}

function endGame() {
	showStatusBar("Oops! Let's try again");
	ship.removeAttribute('forward');
	setInterval(showCounter, 1000);
	setTimeout(reloadGame, 4500);
}

function showCounter() {
	if (counter == 0) return;
	showStatusBar('Retrying in ' + counter);
	counter--;	
}

function reloadGame() {	
	location.reload();
}