let wipeText = _ => {
	statusText.setAttribute('text', 'value', '');
}

let hideStatusBar = _ => {
	statusText.setAttribute('visible', false);
}

let showStatusBar = (text = null) => {
	if (text) statusText.setAttribute('text', 'value', text);
	statusText.setAttribute('visible', true);
}