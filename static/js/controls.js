var inVRMode = false;

let playOrPause = _ => {
  isPaused = !isPaused;
  if (isPaused) play();
  else pause();
};

let enterOrExitVR = _ => {
  if (inVRMode) scene.enterVR();
  else scene.exitVR();
  inVRMode = !inVRMode;
};

let play = _ => {
  hideStatusBar();
  wipeText();
  if (!scoreText.hasAttribute('score')) scoreText.setAttribute('score', '');
  if (!container.hasAttribute('zones')) container.setAttribute('zones', '');
  container.play();
};

let pause = _ => {
  showStatusBar('PAUSED')
  container.pause();
};

let goLeft = _ => {
  var pos = ship.getAttribute('position');      
  pos.x = (pos.x == 2) ? 0 : -2;
  ship.setAttribute('position', pos);
};

let goUp = _ => {
  var pos = ship.getAttribute('position');
  if (pos.y == 3) return;
  pos.y = 3;
  ship.setAttribute('position', pos);
};

let goDown = _ => {
  var pos = ship.getAttribute('position');
  if (pos.y == 1) return;
  pos.y = 1;
  ship.setAttribute('position', pos);
};

let goRight = _ => {
  var pos = ship.getAttribute('position');
  pos.x = (pos.x == -2) ? 0 : 2;
  ship.setAttribute('position', pos);
};

document.addEventListener('keydown', function (e) {

  if (e.which == 32) { /* space */
     playOrPause();
  }

  if (e.which == 37) { /* left */
    if (isPaused == false) return;
    goLeft();
  }
  if (e.which == 38) { /* up */
    if (isPaused == false) return;
    goUp();
  }
  if (e.which == 39) { /* right */
    if (isPaused == false) return;
    goRight();
  }
  if (e.which == 40) { /* down */
    if (isPaused == false) return;
    goDown();
  }
});

container.addEventListener('gamepadbuttondown', function (e) {
  if (e.detail.index == 14) goLeft();
  else if (e.detail.index == 15) goRight();
  else if (e.detail.index == 12) goUp();
  else if (e.detail.index == 13) goDown();
});

emptyContainer.addEventListener('gamepadbuttondown', function (e) {
  if (e.detail.index == 9) playOrPause();
  else if (e.detail.index == 7) enterOrExitVR();
});

function activateShip() {
  ship.setAttribute('forward', '');
  if (isPaused == true) {
    scoreText.setAttribute('score', '');
    container.setAttribute('zones', '');
  }
}

setTimeout(activateShip, 7000);